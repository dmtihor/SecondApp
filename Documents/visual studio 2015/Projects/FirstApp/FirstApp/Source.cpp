#include <iostream>
#include <string>

using namespace std;

int main()
{
	string in_string;
	cout << "input username : ";
	cin >> in_string;
	if (in_string.empty())
		cerr << "error: input string is empty!";
	else cout << "Hello, " << in_string << "!\n";
	system("pause");
}